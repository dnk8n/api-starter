export PROJECT_NAME=api
export DEPLOYMENT_ENVIRONMENT=develop  # develop, staging, production
export PROJECT_ROOT=${PWD}
export PATH="${PROJECT_ROOT}:${PATH}"

# Cloud Provider Secrets
export AWS_ACCESS_KEY=
export AWS_SECRET_KEY=
export VAGRANT_IP=10.180.0.106

# Cloud Provider Variables
export PRIMARY_USER=ubuntu

# Container Secrets
export ACME_EMAIL=
export GANDIV5_API_KEY=
export TRAEFIK_BASICAUTH_USERS=''

# Container Variables
export DOCKER_PGID=$(grep docker /etc/group | cut -d ':' -f 3)
export DOMAIN=${PROJECT_NAME}.lan
export DOMAIN_FASTAPI=${DOMAIN}
export DOMAIN_FRONTEND=${DOMAIN}
export DOMAIN_TRAEFIK=router.${DOMAIN}
export DOMAIN_NETDATA=monitor.${DOMAIN}
export DOMAIN_PROMETHEUS=prom.${DOMAIN}
export DOMAIN_GRAFANA=grafana.${DOMAIN}
export DOMAIN_MAILHOG=mail.${DOMAIN}
export DOMAIN_PGADMIN=pgadmin.${DOMAIN}
export DOMAIN_FLOWER=flower.${DOMAIN}

# Application Secrets
export DEFAULT_ADMIN_PASSWORD=${PROJECT_NAME}
export POSTGRES_PASSWORD=${DEFAULT_ADMIN_PASSWORD}
export PGADMIN_DEFAULT_PASSWORD=${DEFAULT_ADMIN_PASSWORD}
export FASTAPI_FIRST_SUPERUSER_PASSWORD=${DEFAULT_ADMIN_PASSWORD}
export FASTAPI_SECRET_KEY=dummy
export EMAIL_HOST=mailhog
export EMAIL_PORT=1025
export EMAIL_HOST_USER=
export EMAIL_HOST_PASSWORD=

# Application Variables
export POSTGRES_USER=${PROJECT_NAME}
export POSTGRES_HOST=postgres
export POSTGRES_PORT=5432
export POSTGRES_DB=${PROJECT_NAME}
export DATABASE_URL=postgres://${POSTGRES_USER}:${POSTGRES_PASSWORD}@${POSTGRES_HOST}:${POSTGRES_PORT}/${POSTGRES_DB}
export DOCKER_IMAGE_CELERY=${PROJECT_NAME}-celery
export DOCKER_IMAGE_CELERY_TAG=$(git branch --show-current)
export DOCKER_IMAGE_FASTAPI=${PROJECT_NAME}-fastapi
export DOCKER_IMAGE_FASTAPI_TAG=$(git branch --show-current)
export DOCKER_IMAGE_FRONTEND=${PROJECT_NAME}-frontend
export DOCKER_IMAGE_FRONTEND_TAG=$(git branch --show-current)
export PGADMIN_DEFAULT_EMAIL=admin@${DOMAIN}

# Terraform Variables
export TF_VAR_instance_type=t3a.medium
export TF_VAR_region=eu-west-1
export TF_VAR_ami_name_regex="base_*"
export TF_VAR_instance_name=${PROJECT_NAME}
export TF_VAR_elastic_ip=
