# api-starter

## Vagrant Development Environment

Vagrant was chosen to make the development environment as close to production as possible and also to minimise differences
between operating systems. Mkcert is used to allow ssl to be valid on development system browsers (Do not use mkcert outside 
development). It is possible to run on a GNU/Linux machine locally using docker-compose if docker is installed, skipping the need for most of the dependencies below.

1. Install common dependencies (once-off):
   - [Direnv](https://direnv.net/)
   - [mkcert](https://github.com/FiloSottile/mkcert) (tip: jump to `build from source (requires Go 1.13+)`)
   - mkcert certificates: `mkcert -install`

1. Install dependencies only needed if using Vagrant (once-off):
   - [Ansible](https://docs.ansible.com/)
   - Ansible requirements: `ansible-galaxy install -r config/ansible/requirements.yml`
   - [Vagrant](https://www.vagrantup.com/)
   - [vagrant-hosts plugin](https://github.com/oscar-stack/vagrant-hosts): `vagrant plugin install vagrant-hosts`
   - [python-netaddr](https://netaddr.readthedocs.io/): `sudo dnf install python-netaddr` or `sudo apt install python-netaddr`

1. Setup project variables (mostly once-off):
   - cd to project root
   - `cp .envrc.tpl .envrc` and modify .envrc with appropriate values (modifications are optional for dev)
   - `direnv allow .` - run this command each time .envrc is modified

> From now on every time you cd to project, your environment variables will be sourced correctly (if Direnv was installed correctly, ensure you apply the hook into shell step)

1. Perform other once-off project operations on host (i.e. Not inside Vagrant machine):
  - `cd ${PROJECT_ROOT}/services/traefik/certs` (`mkdir ${PROJECT_ROOT}/services/traefik/certs` first if directory doesn't yet exist)
  - `mkcert -cert-file local.crt -key-file local.key "${DOMAIN}" "*.${DOMAIN}"` - once-off
    Note: If certs are regenerated while traefik is already up, `dc restart traefik`
  - `sudo nano /etc/hosts` and copy in block resulting from the following command (if possible, skip duplicates that are a potential):
   ```
   printf "\
   ${VAGRANT_IP} ${DOMAIN}\n\
   ${VAGRANT_IP} ${DOMAIN_FASTAPI}\n\
   ${VAGRANT_IP} ${DOMAIN_FRONTEND}\n\
   ${VAGRANT_IP} ${DOMAIN_TRAEFIK}\n\
   ${VAGRANT_IP} ${DOMAIN_NETDATA}\n\
   ${VAGRANT_IP} ${DOMAIN_PROMETHEUS}\n\
   ${VAGRANT_IP} ${DOMAIN_GRAFANA}\n\
   ${VAGRANT_IP} ${DOMAIN_MAILHOG}\n\
   ${VAGRANT_IP} ${DOMAIN_PGADMIN}\n\
   ${VAGRANT_IP} ${DOMAIN_FLOWER}\n\
   "
   ```

1. Perform vagrant steps (only if using Vagrant)
  - `vagrant up`
  - `vagrant ssh`
  - `cd src`
  - `direnv allow .` - each environment variable change

> When current working directory is within the project's tree, `dc` can be used synonomously with `docker-compose`, however depending on the `${DEPLOYMENT_ENVIRONMENT}`, the appropriate file overrides will be made making commands less prone to error. It is also not a requirement to first cd to `${PROJECT_ROOT}` in order to run a docker-compose command

1. `dc build` - each code change

1. `dc up -d` (Optionally preconfigure database and/or volumes before running this step, see below)

1. Optionally:

   - Setup database:
     - `dc up -d postgres`
     - To restore database dump: `gunzip < /path/to/api.<timestamp>.pg.dump.sql.gzip | dc exec -T postgres psql -U ${POSTGRES_USER} ${POSTGRES_DB}`
   Note: 
   To create a database dump on dev: `dc exec -T postgres pg_dump -U ${POSTGRES_USER} -O -x ${POSTGRES_DB} | gzip -9 > /path/to/api-dev.$(date '+%Y%m%d-%H%M').pg.dump.sql.gzip`

1. All routing can be found at `https://${DOMAIN_TRAEFIK}/dashboard/#/http/routers` for example
